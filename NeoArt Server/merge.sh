#!/bin/bash
# neoart - HCI Project 2016

userID=$1
styleImage="inputs/style_$userID.png"
contentImage="inputs/photo_$userID.png"
outputImage="outputs/$userID.png"

cd neural-style-master
th neural_style.lua -style_image $styleImage -content_image $contentImage -output_image $outputImage -init image -num_iterations 300 -gpu -1
