package ch.usi.neoart.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Author: Lara Bruseghini
 * HCI Atelier Project - Spring 2016
 */

class ImageReceiverServer implements Runnable{

    private class ReceiverConnectionHandler implements Runnable{
        Socket clientSocket;

        public ReceiverConnectionHandler(Socket clientSocket){
            this.clientSocket = clientSocket;
        }

        @Override
        public void run(){
            try {
                logger.log(Level.INFO, "ImageReaderServer running");
                InputStream socketInputStream = socket.getInputStream();
                DataInputStream dataInStream = new DataInputStream(socketInputStream);
                String clientID = dataInStream.readUTF();
                logger.log(Level.INFO, "Received client ID: " + clientID);

                int photoSize = dataInStream.readInt();
                byte[] photoData = new byte[photoSize];
                dataInStream.readFully(photoData, 0 , photoSize);
                logger.log(Level.INFO, "Received photo image");

                int styleSize = dataInStream.readInt();
                byte[] styleData = new byte[styleSize];
                dataInStream.readFully(styleData, 0 , styleSize);
                logger.log(Level.INFO, "Photo and style images received successfully");

                OutputStream out = new FileOutputStream(
                        NeoArtServer.INPUT_IMAGE_FOLDER + URLEncoder.encode("photo_"+clientID, "UTF-8") + ".png");
                out.write(photoData);
                out.flush();
                out = new FileOutputStream(
                        NeoArtServer.INPUT_IMAGE_FOLDER + URLEncoder.encode("style_"+clientID, "UTF-8") + ".png");
                out.write(styleData);
                out.close();

                neoArtServer.enqueue(clientID);
                logger.log(Level.INFO, "Client request queued successfully.");
            }catch(Exception e){
                logger.log(Level.SEVERE, "Error receiving images with ImageReceiverServer.", e);
            }
        }
    }


    private static final Logger logger = Logger.getLogger("ImageReceiverServer");

    private ServerSocket server;
    private Socket socket;
    private boolean running;

    NeoArtServer neoArtServer;

    public ImageReceiverServer(int port, NeoArtServer neoArtServer){
        this.neoArtServer = neoArtServer;

        try{
            server = new ServerSocket(port);
            running = true;
            logger.log(Level.INFO, "ImageReceiverServer listening on port "+ port);
            new Thread(this).start();
        }catch(Exception e){
            logger.log(Level.SEVERE, "An error occurred while initialising ImageReceiverServer.", e);
        }
    }

    public void stopExecution(){
        running = false;
    }

    @Override
    public void run() {
        try {
            while(running){
                socket = server.accept();
                new Thread(new ReceiverConnectionHandler(socket)).start();
            }
        }catch(Exception e){
            logger.log(Level.SEVERE, "An error occurred while receiving images.", e);
        } finally {
            try {
                socket.close();
            }catch(Exception e){
                logger.log(Level.SEVERE, "An error occurred while closing the ImageReaderServer socket.", e);
            }
        }
    }
}
