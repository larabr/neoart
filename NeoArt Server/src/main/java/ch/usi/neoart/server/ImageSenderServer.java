package ch.usi.neoart.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Author: Lara Bruseghini
 * HCI Atelier Project - Spring 2016
 */

public class ImageSenderServer implements Runnable{

    private class SenderConnectionHandler implements Runnable{
        Socket clientSocket;

        public SenderConnectionHandler(Socket clientSocket){
            this.clientSocket = clientSocket;
        }

        @Override
        public void run(){
            try {
                logger.log(Level.INFO, "Client connected to ImageSenderServer");
                InputStream socketInputStream  = clientSocket.getInputStream();
                OutputStream socketOutputStream = clientSocket.getOutputStream();

                DataOutputStream dataOutStream = new DataOutputStream(socketOutputStream);
                DataInputStream dataInStream = new DataInputStream(socketInputStream);

                String clientID = dataInStream.readUTF();
                logger.log(Level.INFO, "Received client ID: " + clientID);

                File image = new File(NeoArtServer.OUTPUT_IMAGE_FOLDER + URLEncoder.encode(clientID, "UTF-8") + ".png");
                byte[] byteArray = Files.readAllBytes(image.toPath());

                //send image
                logger.log(Level.INFO, "image size: "+ byteArray.length);
                dataOutStream.writeInt(byteArray.length);
                dataOutStream.write(byteArray, 0, byteArray.length);
                logger.log(Level.INFO, "Image sent.");
            }catch(Exception e){
                logger.log(Level.SEVERE, "Error sending result image with ImageSenderServer.", e);
            }
        }
    }

    private static final Logger logger = Logger.getLogger("ImageSenderServer");

    private ServerSocket server;
    private boolean running;

    public ImageSenderServer(int port){
        try {
            server = new ServerSocket(port);
            logger.log(Level.INFO, "ImageSenderSever listening on port " + port);
            running = true;
            new Thread(this).start();
        }catch(Exception e){
            logger.log(Level.SEVERE, "Error initialising ImageSenderServer.", e);
        }
    }

    public void stopExecution(){
        running = false;
    }

    @Override
    public void run() {
        while (running) {
            try {
                Socket socket = server.accept();
                logger.log(Level.INFO, "ImageSenderSever incoming connection accepted.");
                new Thread(new SenderConnectionHandler(socket)).start();
            }catch(Exception e){
                logger.log(Level.SEVERE, "Error initialising ImageSenderServer.", e);
            }
        }
    }
}
