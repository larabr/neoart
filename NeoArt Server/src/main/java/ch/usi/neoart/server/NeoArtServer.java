/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.usi.neoart.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import java.net.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * NeoArtServer provides the logic to allow clients to register and be notified of other
 * clients registering. It also responds to pings from clients.
 */

public class NeoArtServer {

  private class Client {
    @SerializedName("registration_token")
    public String registrationToken;

    public Client(String ID){
      registrationToken = ID;
    }
  }


    /**
     * Handles lua script execution 
     */
  private class MergeQueueManager implements Runnable{
    private final ConcurrentLinkedQueue<Client> Q;
    private final NeoArtServer neoArtServer;
    private boolean running;

    private MergeQueueManager(NeoArtServer neoArtServer) {
      this.neoArtServer = neoArtServer;
      Q = new ConcurrentLinkedQueue<>();
      running = true;
      new Thread(this).start();
    }

    public void enqueue(Client c){
      Q.add(c);
    }

    @Override
    public void run() {
      Client c;

      while(running) {
        if ((c = Q.poll()) != null) {
          try {
            Process merge = new ProcessBuilder("./merge.sh", URLEncoder.encode(c.registrationToken, "UTF-8")).start();
            merge.waitFor();
            logger.log(Level.INFO, "Merge processed successfully for client " + c.registrationToken);

            // notify client that output is ready
            JsonObject jMsg = new JsonObject();
            JsonObject jSend = new JsonObject();
            jMsg.addProperty("action", "result_info");
            jMsg.addProperty("id", c.registrationToken);
            if (SERVER_IP != null)
                 jMsg.addProperty("message", SERVER_IP + ":" + IMAGE_SENDER_PORT);
            else
                 jMsg.addProperty("message", "An error occurred while processing your request");
            jSend.add("data", jMsg);
            neoArtServer.getGcmServer().send(c.registrationToken, jSend);


          } catch (Exception e) {
            logger.log(Level.SEVERE, "Error running merge script", e);
          }
        }

        if (Thread.interrupted()) {
          return;
        }
      }
    }
  }



  // FriendlyGcmServer defines onMessage to handle incoming friendly ping messages.
  private class NeoGcmServer extends GcmServer {

    public NeoGcmServer (String apiKey, String senderId) {
      super(apiKey, senderId);
    }


    @Override
    public void onMessage(String from, JsonObject jData) {

      if (jData.has("action") && jData.get("action").getAsString().equals("upload_images")) {

          JsonObject jMsg = new JsonObject();
          JsonObject jSend = new JsonObject();
          jMsg.addProperty("action", "server_info");
          jMsg.addProperty("id", from);
          if (SERVER_IP != null)
              jMsg.addProperty("message", SERVER_IP + ":" + IMAGE_READER_PORT);
          else
              jMsg.addProperty("message", "An error occurred while processing your request");
          jSend.add("data", jMsg);
          send(from, jSend);

      }

    }
  }





  //private static final String SERVER_IP = GcmServer.getIp(); //@TODO cannot work with public address without config the router
    /**
     * Open firewall port:
     * /etc/pf.conf << "pass in proto tcp from any to any port 30000"
     * sudo pfctl -vnf /etc/pf.conf
     */
  private static final String SERVER_IP = "10.62.129.177";
  //private static final String SERVER_IP = "192.168.1.116";
  private static final int IMAGE_READER_PORT = 30000;
  private static final int IMAGE_SENDER_PORT = 30001;
  private static final String SENDER_ID = "877949200105";
  private static final String SERVER_API_KEY = "AIzaSyCxp1MDb91kZWK8o8Hd2BYQ5-dibtg9Zws";
  public static final String INPUT_IMAGE_FOLDER = "neural-style-master/inputs/";
  public static final String OUTPUT_IMAGE_FOLDER = "neural-style-master/outputs/";
  private static final Logger logger = Logger.getLogger("NeoArtServer");

  // Listener responsible for handling incoming registrations and pings.
  private NeoGcmServer neoGcmServer;
  // Gson helper to assist with going to and from JSON and Client.
  private Gson gson;
  private MergeQueueManager queueManager;


  public NeoArtServer(String apiKey, String senderId) {
    gson = new GsonBuilder().create();
    neoGcmServer = new NeoGcmServer(apiKey, senderId);
    queueManager = new MergeQueueManager(this);

    new ImageReceiverServer(IMAGE_READER_PORT, this);
    new ImageSenderServer(IMAGE_SENDER_PORT);
  }


  public void enqueue(String clientID){
      queueManager.enqueue(new Client(clientID));
  }

  public NeoGcmServer getGcmServer() {
        return neoGcmServer;
  }

    public static void main(String[] args) {
    // Initialize NeoArtServer with appropriate API Key and SenderID.
    new NeoArtServer(SERVER_API_KEY, SENDER_ID);

    // Keep main thread alive.
    try {
      CountDownLatch latch = new CountDownLatch(1);
      latch.await();
    } catch (InterruptedException e) {
      logger.log(Level.SEVERE, "An error occurred while latch was waiting.", e);
    }
  }
}
