package ch.usi.neoart;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;
import android.view.WindowManager;

import ch.usi.neoart.server.MyGcmListenerService;

public class CreateArtworks extends AppCompatActivity
        implements
        TabPhoto.OnPhotoSelectedListener,
        TabPainting.OnPaintingSelectedListener,
        TabConfirm.OnConfirmSelectedListener,
        MyGcmListenerService.OnRequestSuccessfulListener{

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private String photoPath, paintingPath;
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_artworks);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    /** Tab fragments communication
     * @see https://developer.android.com/training/basics/fragments/communicating.html */
    /**
     * Fetch and save image selected in TabPhoto
     * @param photoPath
     */
    @Override
    public void setPhotoSelected(String photoPath) {
        Log.d("INFO", "saving images");
        this.photoPath = photoPath;
    }

    /**
     * Fetch and save image selected in TabPainting
     * @param paintingPath
     */
    @Override
    public void setPaintingSelected(String paintingPath) {
        this.paintingPath = paintingPath;
    }

    /**
     * For TabConfirm to access selected images in TabPhoto and TabPainting
     * @return [photoUri, paintingUri]
     */
    @Override
    public String[] retrieveSelectedImages() {
        Log.d("INFO", "Returning selected images");
        return new String[]{photoPath, paintingPath};
    }

    @Override
    public void onRequestSuccessful() {

       runOnUiThread(new Runnable() {
                public void run() {
                    int confirmIndex = mSectionsPagerAdapter.TAB_CONFIRM_INDEX;
                    TabConfirm fragment = (TabConfirm) mSectionsPagerAdapter.getRegisteredFragment(confirmIndex);
                    if(null != fragment)
                        fragment.changeHue();
                    else
                        Log.i("INFO", "Fragment is null");

                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            Intent intent = new Intent(CreateArtworks.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }, 2000);

                }
            });
    }
    /* End Tab fragments communication */

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        public final int TAB_PHOTO_INDEX = 0;
        public final int TAB_PAINTING_INDEX = 1;
        public final int TAB_CONFIRM_INDEX = 2;

        @Override
        public Fragment getItem(int position) {
            switch (position) {

                case TAB_PHOTO_INDEX:
                    return new TabPhoto();
                case TAB_PAINTING_INDEX:
                    return new TabPainting();
                case TAB_CONFIRM_INDEX:
                    return new TabConfirm();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Photo";
                case 1:
                    return "Painting";
                case 2:
                    return "NeoMerge";
            }
            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }
        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
    }
}
