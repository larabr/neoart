package ch.usi.neoart;

/**
 * Created by lara on 02/06/16.
 */
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.chyrta.onboarder.OnboarderActivity;
import com.chyrta.onboarder.OnboarderPage;
import java.util.ArrayList;
import java.util.List;

public class HelpActivity extends OnboarderActivity {

    List<OnboarderPage> onboarderPages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onboarderPages = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimaryDark));
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Each page of the tutorial is created here
        OnboarderPage onboarderPage1 = new OnboarderPage("NeoArt", "Merge photos and paintings to bring art to a brand new level",R.drawable.help_intro);
        OnboarderPage onboarderPage2 = new OnboarderPage("Create Artwork", "You can start by clicking the red button or selecting the option in the menu", R.drawable.help_fab);
        OnboarderPage onboarderPage3 = new OnboarderPage("Pick images", "Select the photo and artwork you want to merge. You can browse the gallery or use the camera.", R.layout.help_pick);
     //   OnboarderPage onboarderPage4 = new OnboarderPage(R.string.map_title, R.string.map_description, R.drawable.map);

        // Onboarder 1: settings
        onboarderPage1.setTitleColor(R.color.white);
        onboarderPage1.setDescriptionColor(R.color.white);
        onboarderPage1.setBackgroundColor(R.color.colorPrimary);
        onboarderPage1.setTitleTextSize(30);
        onboarderPage1.setDescriptionTextSize(19);

         // Onboarder 2: settings
         onboarderPage2.setTitleColor(R.color.white);
         onboarderPage2.setDescriptionColor(R.color.white);
         onboarderPage2.setBackgroundColor(R.color.colorPrimary);
         onboarderPage2.setTitleTextSize(30);
         onboarderPage2.setDescriptionTextSize(19);

        // Onboarder 3: settings
  //      onboarderPage3.setTitleColor(R.color.white);
   //     onboarderPage3.setDescriptionColor(R.color.white);
   //     onboarderPage3.setBackgroundColor(R.color.colorPrimarySettings);
    //    onboarderPage3.setTitleTextSize(30);
    //    onboarderPage3.setDescriptionTextSize(19);

        // Onboarder 4: settings
   //     onboarderPage4.setTitleColor(R.color.white);
   //     onboarderPage4.setDescriptionColor(R.color.white);
   //     onboarderPage4.setBackgroundColor(R.color.colorPrimarySettings);
   //     onboarderPage4.setTitleTextSize(30);
   //     onboarderPage4.setDescriptionTextSize(19);

        // Each page is added to an ArrayList of pages.
        onboarderPages.add(onboarderPage1);
        onboarderPages.add(onboarderPage2);
        onboarderPages.add(onboarderPage3);
//        onboarderPages.add(onboarderPage4);

        setOnboardPagesReady(onboarderPages);
    }

    @Override
    public void onSkipButtonPressed() {
        // Optional: by default it skips onboarder to the end
        super.onSkipButtonPressed();
        dismiss();
    }

    @Override
    public void onFinishButtonPressed() {
        dismiss();
    }

    private void dismiss() {
        Intent resultIntent = new Intent();
        setResult(RESULT_OK, resultIntent);
        finish();
    }
}
