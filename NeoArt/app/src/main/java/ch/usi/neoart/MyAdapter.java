package ch.usi.neoart;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    public static ArrayList<String> mDataset;
    ArrayList<String> itemsPendingRemoval;
    private Handler handler = new Handler(); // hanlder for running delayed runnables
    HashMap<String, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables, so we can cancel a removal if need be
    private static final int PENDING_REMOVAL_TIMEOUT = 3000; // 3sec

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView image;
        public Button btn;


        public ViewHolder(View v) {
            super(v);
            image = (ImageView)v.findViewById(R.id.imgPreview);
            btn = (Button)v.findViewById(R.id.btn_share);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    share(mDataset.get(getLayoutPosition()));
                }
            });

        }

        private void share(String path)
        {
            // Create the new Intent using the 'Send' action.
            Intent shareIntent = new Intent(Intent.ACTION_SEND);

            // Set the MIME type
            shareIntent.setType("image/jpg");

            // Create the URI from the media
            File media = new File(path);
            Uri uri = Uri.fromFile(media);

            // Add the URI to the Intent.
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);

            Context cont = itemView.getContext();

            // Broadcast the Intent.
            cont.startActivity(Intent.createChooser(shareIntent, "Share to"));
        }

    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(ArrayList<String> myDataset) {
        mDataset = myDataset;
        itemsPendingRemoval = new ArrayList<>();
    }


    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new TestViewHolder(parent);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Bitmap bitmap = BitmapFactory.decodeFile(mDataset.get(position));

        holder.image.setImageBitmap(bitmap);
        //holder.expandedImageView.setImageBitmap(bitmap);

        final String item = mDataset.get(position);
        final TestViewHolder viewHolder = (TestViewHolder) holder;
        if (itemsPendingRemoval.contains(item)) {
            // we need to show the "undo" state of the row
            // GONE looks better but FAB covers last item
             viewHolder.container.setVisibility(View.GONE);
            //viewHolder.container.setVisibility(View.INVISIBLE);
            viewHolder.itemView.setBackgroundColor(Color.RED);
            viewHolder.titleTextView.setVisibility(View.GONE);
            viewHolder.undoButton.setVisibility(View.VISIBLE);
            viewHolder.undoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // user wants to undo the removal, let's cancel the pending task
                    Runnable pendingRemovalRunnable = pendingRunnables.get(item);
                    pendingRunnables.remove(item);
                    if (pendingRemovalRunnable != null) handler.removeCallbacks(pendingRemovalRunnable);
                    itemsPendingRemoval.remove(item);
                    // this will rebind the row in "normal" state

                    notifyItemChanged(position);
                    //viewHolder.container.setBackgroundColor(Color.WHITE);
                }
            });
        } else {
            // we need to show the "normal" state

            viewHolder.itemView.setBackgroundColor(Color.WHITE);
            viewHolder.titleTextView.setVisibility(View.INVISIBLE);
            viewHolder.titleTextView.setText(item);
            viewHolder.undoButton.setVisibility(View.GONE);
            viewHolder.undoButton.setOnClickListener(null);
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void pendingRemoval(int position) {
        final String item = mDataset.get(position);
        if (!itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.add(item);
            // this will redraw row in "undo" state
            notifyItemChanged(position);
            // let's create, store and post a runnable to remove the item
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    remove(mDataset.indexOf(item));
                }
            };
            handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
            pendingRunnables.put(item, pendingRemovalRunnable);
        }
    }

    public void remove(int position) {
        String item = mDataset.get(position);
        if (itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.remove(item);
        }
        if (mDataset.contains(item)) {
            mDataset.remove(position);
            notifyItemRemoved(position);
            //Log.i("INFO", "Removed item: " + item);
            if (new File(item).delete())
                Log.d("INFO", item + " deleted successfully");
            else
                Log.d("ERROR", "Error deleting " + item);
        }
    }

    public boolean isPendingRemoval(int position) {
        String item = mDataset.get(position);
        return itemsPendingRemoval.contains(item);
    }

    /**
     * ViewHolder capable of presenting two states: "normal" and "undo" state.
     */
     class TestViewHolder extends MyAdapter.ViewHolder {

        TextView titleTextView;
        Button undoButton;
        LinearLayout container;
        FrameLayout deleteContainer;

        public TestViewHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.content_main_card, parent, false));
            container = (LinearLayout) itemView.findViewById(R.id.card_container);
            deleteContainer = (FrameLayout) itemView.findViewById(R.id.delete_container);
            titleTextView = (TextView) itemView.findViewById(R.id.title_text_view);
            undoButton = (Button) itemView.findViewById(R.id.undo_button);
        }

    }

}
