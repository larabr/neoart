package ch.usi.neoart;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class RobotoTextView extends TextView{

    public RobotoTextView(Context context, AttributeSet attributes) {
        super(context, attributes);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),
                "fonts/roboto_light.ttf"));
    }
}
