package ch.usi.neoart;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import ch.usi.neoart.server.MyGcmListenerService;

public class TabConfirm extends Fragment implements View.OnClickListener {
    OnConfirmSelectedListener mCallback;

    // Container Activity must implement this interface
    public interface OnConfirmSelectedListener {
        String[] retrieveSelectedImages();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            Activity activity = (Activity) context;
            mCallback = (OnConfirmSelectedListener) activity;
        } catch (ClassCastException e) {
            Log.d("ERROR", "Must implement OnConfirmListener " + e);
        }
    }

    private static final String TAG = "TabConfirm";

    private Button buttonConfirm;
    private String[] selectedImages;
    private ImageView imageConfirmPhoto;
    private ImageView imageConfirmPainting;
    private TextView textView;
    private ProgressBar loadingAnimation;
    private RelativeLayout loadingPanel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_confirm, container, false);

        imageConfirmPhoto = (ImageView) v.findViewById(R.id.imageConfirmPhoto);
        imageConfirmPainting = (ImageView) v.findViewById(R.id.imageConfirmPainting);
        loadingPanel = (RelativeLayout) v.findViewById(R.id.loadingPanel);
        loadingAnimation = (ProgressBar) v.findViewById(R.id.loadingAnimation);
        imageConfirmPhoto.setVisibility(View.VISIBLE);
        imageConfirmPainting.setVisibility(View.VISIBLE);
        loadingPanel.setVisibility(View.INVISIBLE);
        loadingAnimation.setVisibility(View.INVISIBLE);

        buttonConfirm = (Button) v.findViewById(R.id.button_confirm);
        buttonConfirm.setOnClickListener(this);

        textView = (TextView) v.findViewById(R.id.textView);

        previewImages();
        return v;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            previewImages();
        }
    }

    private void previewImages(){
        selectedImages = mCallback.retrieveSelectedImages();

        boolean isPhotoSelected = selectedImages[0] != null;
        boolean isPaintingSelected = selectedImages[1] != null;

        if (isPhotoSelected) {
            // bitmatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();
            // downsizing image as it throws OutOfMemory Exception for larger images
            options.inSampleSize = 8;

            final Bitmap bitmapPhoto = BitmapFactory.decodeFile(selectedImages[0], options);
            imageConfirmPhoto.setImageBitmap(bitmapPhoto);

            textView.setVisibility(View.INVISIBLE);
        }
        if (isPaintingSelected) {

            // bitmatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();
            // downsizing image as it throws OutOfMemory Exception for larger images
            options.inSampleSize = 8;

            final Bitmap bitmapPainting = BitmapFactory.decodeFile(selectedImages[1], options);

            imageConfirmPainting.setImageBitmap(bitmapPainting);

            textView.setVisibility(View.INVISIBLE);
        }

        // when both images are up:
        buttonConfirm.setEnabled(isPhotoSelected && isPaintingSelected);

    }

    @Override
    public void onClick(View v) {

        MyGcmListenerService.bindActivity(getActivity());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("photoPath", selectedImages[0]);
        editor.putString("paintingPath", selectedImages[1]);
        editor.apply();

        new AsyncTask<Void, Void, String>() {


            final Activity activity = getActivity();
            final GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(activity);
            AtomicInteger msgId = new AtomicInteger();
            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try {
                    Bundle data = new Bundle();
                    data.putString("action", "upload_images");
                    String id = Integer.toString(msgId.incrementAndGet());
                    Log.d(TAG, getString(R.string.gcm_defaultSenderId));
                    gcm.send(getString(R.string.gcm_defaultSenderId) + "@gcm.googleapis.com", id, data);
                    msg = "Sent message";


                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.d(TAG, msg);
            }
        }.execute(null, null, null);

        loadingPanel.setVisibility(View.VISIBLE);
        loadingAnimation.setVisibility(View.VISIBLE);
        buttonConfirm.setText(R.string.confirm_loading);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if(loadingAnimation.getVisibility() == View.VISIBLE){
                    loadingAnimation.setVisibility(View.INVISIBLE);
                    loadingPanel.setVisibility(View.INVISIBLE);
                    buttonConfirm.setText(R.string.confirm_failed);
                }
            }
        }, 10000);


    }

    public void changeHue() {
        final Drawable roundDrawable = buttonConfirm.getBackground();
        final float[] from = new float[3],
                to = new float[3];

        Color.colorToHSV(Color.parseColor("#FFD50000"), from);
        Color.colorToHSV(Color.parseColor("#FF00D500"), to);

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.setDuration(300);

        loadingPanel.setVisibility(View.INVISIBLE);
        loadingAnimation.setVisibility(View.INVISIBLE);
        final float[] hsv = new float[3];
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                hsv[0] = from[0] + (to[0] - from[0]) * animation.getAnimatedFraction();
                hsv[1] = from[1] + (to[1] - from[1]) * animation.getAnimatedFraction();
                hsv[2] = from[2] + (to[2] - from[2]) * animation.getAnimatedFraction();
                roundDrawable.setColorFilter(Color.HSVToColor(hsv), PorterDuff.Mode.SRC_ATOP);
                buttonConfirm.setBackground(roundDrawable);
                buttonConfirm.setText(R.string.confirm_success);
            }
        });

        anim.start();
    }

}
