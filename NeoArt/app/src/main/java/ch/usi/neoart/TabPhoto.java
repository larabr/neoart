package ch.usi.neoart;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.app.Activity;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TabPhoto extends Fragment implements View.OnClickListener {
    OnPhotoSelectedListener mCallback;
    // Container Activity must implement this interface
    public interface OnPhotoSelectedListener {
        public void setPhotoSelected(String photoPath);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            Activity activity = (Activity) context;
            mCallback = (OnPhotoSelectedListener) activity;
        } catch (ClassCastException e) {
            Log.d("ERROR", "Must implement OnPhotoSelectedListener " + e);
        }
    }


    private Boolean isFabOpen = false;
    Animation fab_close, fab_open, rotate_backward, rotate_forward;
    FloatingActionButton fabAdd,fabCamera,fabGallery;
    private ImageView imagePhoto;
    private TextView textView;

    private String photoPath;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "NeoArt";
    private static int RESULT_LOAD_IMAGE = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_photo, container, false);
        imagePhoto = (ImageView) v.findViewById(R.id.imagePhoto);
        textView = (TextView) v.findViewById(R.id.textView);
        fabAdd = (FloatingActionButton) v.findViewById(R.id.fabAdd);
        fabGallery = (FloatingActionButton) v.findViewById(R.id.fabGallery);
        fabCamera = (FloatingActionButton) v.findViewById(R.id.fabCamera);
        previewCapturedImage();
        fab_open = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.rotate_backward);
        fabAdd.setOnClickListener(this);
        fabGallery.setOnClickListener(this);
        fabCamera.setOnClickListener(this);
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                // successfully captured the image
                // display it in image view
                previewCapturedImage();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getActivity(), "User cancelled image capture", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Sorry! Failed to capture image", Toast.LENGTH_LONG).show();
            }
        }

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };


            Cursor cursor = getActivity().getApplicationContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            photoPath = cursor.getString(columnIndex);
            cursor.close();

            previewCapturedImage();
        }

    }

    /*
      Capturing Camera Image will lauch camera app requrest image capture
   */
    private void captureImage() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri photoUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

        photoPath = photoUri.getPath();

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void OpenGallery()
    {
        Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.fabAdd:
                animateFAB();
                break;
            case R.id.fabGallery:
                OpenGallery();
                animateFAB();//close
                break;
            case R.id.fabCamera:
                if(isDeviceSupportCamera())
                {
                    captureImage();
                    animateFAB(); //close
                }
                else {
                    Toast.makeText(getActivity(), "Device doesn't have a camera", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void animateFAB(){

        if(isFabOpen){
            fabAdd.startAnimation(rotate_backward);
            fabGallery.startAnimation(fab_close);
            fabCamera.startAnimation(fab_close);
            fabGallery.setClickable(false);
            fabCamera.setClickable(false);
            isFabOpen = false;

        } else {

            fabAdd.startAnimation(rotate_forward);
            fabGallery.startAnimation(fab_open);
            fabCamera.startAnimation(fab_open);
            fabGallery.setClickable(true);
            fabCamera.setClickable(true);
            isFabOpen = true;

        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image
     */
    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        }
        else {
            return null;
        }

        return mediaFile;
    }

    /*
     * Display image from a path to ImageView
     */
    private void previewCapturedImage() {
        if(photoPath != null) {
            try {
                // send URI to activity
                mCallback.setPhotoSelected(photoPath);

                imagePhoto.setVisibility(View.VISIBLE);
                textView.setVisibility(View.INVISIBLE);

                // bitmap factory
                BitmapFactory.Options options = new BitmapFactory.Options();
                final Bitmap bitmap = BitmapFactory.decodeFile(photoPath, options);

                imagePhoto.setImageBitmap(bitmap);


            } catch (NullPointerException e) {
                Log.d("ERROR", "Error in previewCapturedImage: " + e);
            }
        }
    }

    private boolean isDeviceSupportCamera() {
        if (getActivity().getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

}
