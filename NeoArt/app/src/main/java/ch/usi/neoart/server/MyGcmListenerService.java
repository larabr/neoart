/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.usi.neoart.server;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import ch.usi.neoart.MainActivity;
import ch.usi.neoart.R;

public class MyGcmListenerService extends GcmListenerService {
    private static OnRequestSuccessfulListener mCallback;
    // Container Activity must implement this interface
    public interface OnRequestSuccessfulListener {
        public void onRequestSuccessful();
    }

    public static void bindActivity(Activity activity){
        mCallback = (OnRequestSuccessfulListener) activity;
    }

    private void unbindActivity(){
        mCallback = null;
    }

    private static final String TAG = "MyGcmListenerService";
    private String photoPath, paintingPath;

    public void getImagePaths(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        photoPath = prefs.getString("photoPath", "");
        paintingPath = prefs.getString("paintingPath", "");
    }
    private byte[] getPhotoBytes(){
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap photo = BitmapFactory.decodeFile(photoPath, options);
        ByteArrayOutputStream photoByteStream = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, photoByteStream);
        return photoByteStream.toByteArray();
    }

    private byte[] getPaintingBytes(){
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap painting = BitmapFactory.decodeFile(paintingPath, options);
        ByteArrayOutputStream paintingByteStream = new ByteArrayOutputStream();
        painting.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, paintingByteStream);
        return paintingByteStream.toByteArray();
    }
    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        String clientID = data.getString("id");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        String action = data.getString("action");
        Log.d(TAG, "Action: " + action);
        if (action != null && action.equals("server_info") && message != null && message.contains(":")){
            try {
                getImagePaths();
                byte[] photoBytes = getPhotoBytes();
                byte[] paintingBytes = getPaintingBytes();
                //byte[] array = outByteStream.toByteArray();
                String[] serverAddress = message.split(":");
                Socket socket = new Socket(serverAddress[0], Integer.parseInt(serverAddress[1]));
                OutputStream socketOutputStream = socket.getOutputStream();
                DataOutputStream dataOutStream = new DataOutputStream(socketOutputStream);
                //ImageView imageView=(ImageView) findViewById(R.id.imageView1);//EditText et = (EditText) findViewById(R.id.EditText01);
                //Bitmap bmp=((BitmapDrawable)imageView.getDrawable()).getBitmap(); //String str = et.getText().toString();

                dataOutStream.writeUTF(clientID);
                Log.i("INFO", "clientID sent:" + clientID);

                //send photo image
                dataOutStream.writeInt(photoBytes.length);
                dataOutStream.write(photoBytes, 0, photoBytes.length);
                //send style image
                dataOutStream.flush();
                dataOutStream.writeInt(paintingBytes.length);
                dataOutStream.write(paintingBytes, 0, paintingBytes.length);

                // Trigger positive feedback in confirm tab
                if(mCallback != null){
                    mCallback.onRequestSuccessful();
                    unbindActivity();
                }

            }catch(Exception e){
                Log.d("ERROR", "Error sending data: "+ e);
            }
        }

        if (action != null && action.equals("result_info") && message != null && message.contains(":")){
            try {

                String[] serverAddress = message.split(":");
                Socket socket = new Socket(serverAddress[0], Integer.parseInt(serverAddress[1]));
                OutputStream socketOutputStream = socket.getOutputStream();
                InputStream socketInputStream = socket.getInputStream();

                DataOutputStream dataOutStream = new DataOutputStream(socketOutputStream);
                DataInputStream dataInputStream = new DataInputStream(socketInputStream);

                dataOutStream.writeUTF(clientID);
                Log.i("INFO", "clientID sent:" + clientID);

                int resultSize = dataInputStream.readInt();
                byte[] resultData = new byte[resultSize];
                dataInputStream.readFully(resultData, 0 , resultSize);
                Log.i("INFO", "Image received");

                //Use timestamp as fileName
                String fileName = Long.toString(System.currentTimeMillis()/1000) + ".png";
                File pathToPictureDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);


                File neoArtDir = new File(pathToPictureDir, "NeoArt");
                if (!neoArtDir.exists()) {
                    if (neoArtDir.mkdirs())
                        Log.i("INFO", "NeoArt folder created ");
                    else
                        Log.d("ERROR", "Could not create NeoArt folder");
                }

                File outputDir = new File(pathToPictureDir, "NeoArt/output");
                if (!outputDir.exists()) {
                    if (outputDir.mkdirs())
                        Log.i("INFO", "NeoArt/output folder created ");
                    else
                        Log.d("ERROR", "Could not create NeoArt/output folder");
                }

                File resultFile = new File(outputDir, fileName);
                if (resultFile.createNewFile()){
                        FileOutputStream fileOutputStream = new FileOutputStream(resultFile);
                        fileOutputStream.write(resultData);
                        sendNotification("Your image is ready.");
                }else{
                    Log.d("ERROR", "Could not create result image file.");
                }

                // Trigger home update
                if(mCallback != null){
                    mCallback.onRequestSuccessful();
                    unbindActivity();
                }

            }catch(Exception e){
                Log.d("ERROR", "Error receiving data: "+ e);
                sendNotification("An error occurred when processing your image.");
            }
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        //sendNotification(message);
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("neoart")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
